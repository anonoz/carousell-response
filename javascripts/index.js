import _ from 'lodash';
import React from 'react';
import ReactDOM from 'react-dom';
import {App} from './app.js';
// import {TopicCard} from './topic_card.js';

ReactDOM.render(
  <App />,
  document.getElementById("app-container"));
