# An attempt to reduce time complexity on read
class SortedList:
    def __init__(self, element_type, elements=[]):
        self.elements = []
        self.element_ids_sorted = []
        self.elements_count = 0
        self.elements_type = element_type
        for element in elements:
            self.append(element)

    # Let people retrieve items w/o sorting
    def __getitem__(self, index):
        return self.elements[index]

    def index(self, element):
        return self.elements.index(element)

    # Worst case: O(N)
    def append(self, new_element):
        if not type(new_element) == self.elements_type:
            raise TypeError("Accepts only {}" % (self.elements_type))
        new_element_index = self.elements_count
        self.elements[new_element_index:] = [new_element]
        self.elements_count += 1

        if self.elements_count == 1:
            self.element_ids_sorted.append(0)
            return

        for counter, i in enumerate(self.element_ids_sorted):
            if new_element < self.elements[i]:
                self.element_ids_sorted.insert(counter, new_element_index)
                return

        self.element_ids_sorted.append(new_element_index)

    # Check against its immediate neighbour to see if there is a need to sort
    def notify_update(self, index):
        current_pos = self.element_ids_sorted.index(index)

        if (current_pos == 0 and
                self.sorted()[current_pos] <= self.sorted()[current_pos+1]):
            return None

        if (current_pos == (self.elements_count-1) and
                self.sorted()[current_pos] >= self.sorted()[current_pos-1]):
            return None

        if (current_pos != (self.elements_count-1) and
                self.sorted()[current_pos] > self.sorted()[current_pos+1]):
            new_pos = self.elements_count - 1
            for counter, j in enumerate(
                    self.element_ids_sorted[current_pos+1:]):
                if self.elements[index] < self.elements[j]:
                    new_pos = current_pos + counter
                    break
            self.element_ids_sorted.pop(current_pos)
            self.element_ids_sorted.insert(new_pos, index)

        if (current_pos != 0 and
                self.sorted()[current_pos] < self.sorted()[current_pos-1]):
            new_pos = 0
            for counter, j in enumerate(
                    reversed(self.element_ids_sorted[0:current_pos-1])):
                if self.elements[index] > self.elements[j]:
                    new_pos = current_pos - counter - 1
                    break
            self.element_ids_sorted.pop(current_pos)
            self.element_ids_sorted.insert(new_pos, index)

        return 0

    # Read: O(1)
    def sorted(self, offset=0, limit=None):
        if limit is None:
            bound = None
        else:
            bound = offset + limit
        return [self.elements[i]
                for i in self.element_ids_sorted[offset:bound]]

    def reverse_sorted(self, offset=0, limit=None):
        if limit is None:
            bound = None
        else:
            bound = offset + limit
        return [self.elements[i]
                for i in list(reversed(self.element_ids_sorted))[offset:bound]]
