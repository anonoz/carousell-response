from flask import Flask, request, jsonify, render_template
from models.sorted_list import SortedList
from models.topic import Topic

# State
topics = SortedList(Topic, [
    Topic('Dread it, run from it, destiny still arrives.'),
    Topic('I hope they remember you.'),
    Topic('Perfectly balanced, as all things should be.')
])

# API
app = Flask(__name__, static_url_path='/static')


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/topics/', methods=['GET', 'POST'])
def topics_index():
    if request.method == 'GET':
        offset = int(request.args.get('page[offset]', '0'))
        limit = int(request.args.get('page[limit]', '20'))
        topics_to_render = topics.reverse_sorted(offset, limit)
        print(topics_to_render)
        resp = jsonify({
            'links': {
                'self': '/topics/?page[offset]=%d&page[limit]=%d' % (
                    offset, limit)
            },
            'data': [topic_to_json_api_resource(topic, topics.index(topic))
                     for topic in topics_to_render]
        })
        resp.headers['Content-Type'] = 'application/vnd.api+json'
        return resp
    elif request.method == 'POST':
        json_data = request.get_json(force=True)
        new_topic_attributes = json_data['data']['attributes']
        new_topic = Topic(new_topic_attributes['content'])
        topics.append(new_topic)
        resp = jsonify({
            'data': topic_to_json_api_resource(
                new_topic, topics.index(new_topic))
        })
        resp.headers['Content-Type'] = 'application/vnd.api+json'
        return resp


@app.route('/topics/<int:topic_id>/upvote', methods=['POST'])
def topic_upvote(topic_id):
    upvoting_topic = topics[topic_id]
    upvoting_topic.upvote()
    topics.notify_update(topic_id)
    resp = jsonify({
        'data': topic_to_json_api_resource(upvoting_topic, topic_id)
    })
    resp.headers['Content-Type'] = 'application/vnd.api+json'
    return resp


@app.route('/topics/<int:topic_id>/downvote', methods=['POST'])
def topic_downvote(topic_id):
    downvoting_topic = topics[topic_id]
    downvoting_topic.downvote()
    topics.notify_update(topic_id)
    resp = jsonify({
        'data': topic_to_json_api_resource(downvoting_topic, topic_id)
    })
    resp.headers['Content-Type'] = 'application/vnd.api+json'
    return resp


# private
def topic_to_json_api_resource(topic, id):
    return {
        'type': 'topics',
        'id': id,
        'attributes': topic.attributes()
    }


# Development
if __name__ == "__main__":
    app.run(debug=True)
