def generate_topic(c):
    request_payload = {
        "data": {
            "type": "topics",
            "attributes": {
                "content": "Lorem ipsum dolor sit amet."
            }
        }
    }
    rv = c.post('/topics/',
                headers={
                    'Content-Type': 'application/vnd.api+json',
                    'Accept': 'application/vnd.api+json'
                }, json=request_payload)
