# Hello there.

[![CircleCI](https://circleci.com/gh/anonoz/project-d2232a/tree/master.svg?style=svg)](https://circleci.com/gh/anonoz/project-d2232a/tree/master) [![Maintainability](https://api.codeclimate.com/v1/badges/c5fe257a2eebe17d7f59/maintainability)](https://codeclimate.com/github/anonoz/project-d2232a/maintainability) [![Deploy to Heroku](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

This repo is a response to a coding challenge given by a company I am intending to join. 

To the engineers that will be going through the code, first things first:

1. I know that you guys use Python & Go for backend internally. I am very inexperienced in using both languages. Nevertheless I try to dig around Python, Flask, and I have expected to spend way more time than what's advised in the e-mail.
2. I have not used React at work before. Though I got a bit of experience with Angular.

App is live on <https://fierce-sierra-24940.herokuapp.com/>. 

## Ground rules

1. Strives to be JSON API v1.0 compliant, with support for pagination. (Didn't finish pagination links though)

## Development environment setup

Assuming Python 3.6, pip, NodeJS v10.6.0 are already available in the system.

```bash
# In this project we use pipenv to manage dependencies and virtual environment to do development in. Skip if pipenv is already available
$ pip install pipenv

# Once pipenv is installed
$ pipenv install --dev
$ pipenv shell
$ python app.py
```

In case Python 3.6 isn't yet available in the system, I recommend installing it thru miniconda: <https://conda.io/miniconda.html>.

To run webpack and do JS development, use another terminal:

```bash
# In this case we assume we use nvm to manage Node engine versions
$ nvm install 10.6
$ npm install
$ npm run dev
```

Webpack will then watch & rebuild the javascripts on the fly whenever there are any changes. You may need to `Shift + cmd + R` to clear the cache when refreshing.

## References

1. Heroku - <https://devcenter.heroku.com/articles/getting-started-with-python>
2. Python class be sortable - <https://stackoverflow.com/questions/7152497/making-a-python-user-defined-class-sortable-hashable>
