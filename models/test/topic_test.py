from models.topic import Topic


def test_sort():
    top_topic = Topic('Thanos did nothing wrong')
    mid_topic = Topic('Thanos is wrong')
    bad_topic = Topic('Tabs not spaces')

    for x in range(0, 100):
        top_topic.upvote()

    for x in range(0, 50):
        mid_topic.upvote()

    for x in range(0, 25):
        bad_topic.upvote()

    topics = [mid_topic, bad_topic, top_topic]
    topics.sort()
    topics.reverse()

    assert topics == [top_topic, mid_topic, bad_topic]
