import _ from 'lodash';

export default function fetchTopics(optargs = {}) {
  options = _.merge({
    offset: 0,
    limit: 20
  }, optargs);

  return fetch(`/topics/?page[offset]=${options.offset}`, {
    headers: new Headers({
      'Accept': 'application/vnd.api+json'
    })
  });
}
