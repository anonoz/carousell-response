from models.sorted_list import SortedList
from models.topic import Topic
import pytest


def test_getitem():
    lucky_numbers = [54, 12, 23, 18, 6, 68, 27]
    sorted_numbers = SortedList(int, lucky_numbers)
    assert sorted_numbers[1] == lucky_numbers[1]


def test_sorted():
    lucky_numbers = [54, 12, 23, 18, 6, 68, 27]
    sorted_numbers = SortedList(int, lucky_numbers)
    assert sorted_numbers.sorted() == sorted(lucky_numbers)


def test_insert_not_accepting_element_of_diff_type():
    sorted_numbers = SortedList(int, [])
    with pytest.raises(TypeError) as e_info:
        sorted_numbers.append('a')


def test_notify_update():
    sorted_topics = SortedList(Topic, [
        Topic('Dread it, run from it, destiny still arrives.'),
        Topic('I hope they remember you.'),
        Topic('Perfectly balanced, as all things should be.')
    ])
    sorted_topics[1].upvote()
    sorted_topics.notify_update(1)
    assert sorted_topics.sorted()[-1].content == 'I hope they remember you.'


def test_reverse_sorted():
    lucky_numbers = [54, 12, 23, 18, 6, 68, 27]
    sorted_numbers = SortedList(int, lucky_numbers)
    assert sorted_numbers.reverse_sorted() == [68, 54, 27, 23, 18, 12, 6]


def test_notify_update_and_reverse_sorted():
    sorted_topics = SortedList(Topic, [
        Topic('Dread it, run from it, destiny still arrives.'),
        Topic('I hope they remember you.'),
        Topic('Perfectly balanced, as all things should be.')
    ])
    sorted_topics[1].upvote()
    sorted_topics.notify_update(1)
    assert(sorted_topics.reverse_sorted()[0].content
           == 'I hope they remember you.')
