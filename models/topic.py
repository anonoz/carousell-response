from functools import total_ordering


@total_ordering
class Topic:
    def __init__(self, content):
        self.content = content
        self.karma = 0

    def __repr__(self):
        return ('%s (%d)' % (self.content, self.karma))

    def attributes(self):
        return {
            'content': self.content,
            'karma': self.karma
        }

    def upvote(self):
        self.karma += 1

    def downvote(self):
        self.karma -= 1

    def __lt__(self, other):
        return self.karma < other.karma
