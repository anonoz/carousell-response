from app import app
from helpers import *

topics_generated = 3


def test_app_topics_create():
    with app.test_client() as c:
        request_payload = {
            "data": {
                "type": "topics",
                "attributes": {
                    "content": "Lorem ipsum dolor sit amet."
                }
            }
        }
        rv = c.post('/topics/',
                    headers={
                        'Content-Type': 'application/vnd.api+json',
                        'Accept': 'application/vnd.api+json'
                    }, json=request_payload)
        global topics_generated
        topics_generated += 1
        json_data = rv.get_json()
        assert json_data['data']['type'] == 'topics'
        content = json_data['data']['attributes']['content']
        assert content == "Lorem ipsum dolor sit amet."
        assert json_data['data']['attributes']['karma'] == 0


def test_app_topics_index():
    with app.test_client() as c:
        # Populate data
        for i in range(0, 5):
            generate_topic(c)
            global topics_generated
            topics_generated += 1
        # Fetch it
        rv = c.get('/topics/',
                   headers={
                       'Accept': 'application/vnd.api+json'
                   })
        json_data = rv.get_json(force=True)
        assert len(json_data['data']) >= 5


def test_app_topics_upvote():
    with app.test_client() as c:
        # Populate data
        for i in range(0, 5):
            generate_topic(c)
            global topics_generated
            topics_generated += 1
        # Do an upvote
        c.post('/topics/2/upvote',
               headers={'Accept': 'application/vnd.api+json'})
        # Check index sorting
        rv = c.get('/topics/',
                   headers={'Accept': 'application/vnd.api+json'})
        json_data = rv.get_json(force=True)
        assert json_data['data'][0]['id'] == 2


def test_app_topics_downvote():
    with app.test_client() as c:
        # Populate data
        for i in range(0, 5):
            generate_topic(c)
            global topics_generated
            topics_generated += 1
        # Do an upvote
        c.post('/topics/4/downvote',
               headers={'Accept': 'application/vnd.api+json'})
        # Check index sorting
        rv = c.get('/topics/',
                   headers={'Accept': 'application/vnd.api+json'})
        json_data = rv.get_json(force=True)
        assert json_data['data'][-1]['id'] == 4


def test_app_topics_index_offset():
    with app.test_client() as c:
        # Populate data
        for i in range(0, 125):
            generate_topic(c)
            global topics_generated
            topics_generated += 1
        last_page_offset = (topics_generated//20) * 20
        # Fetch it
        rv = c.get('/topics/?page[offset]={}'.format(last_page_offset),
                   headers={
                       'Accept': 'application/vnd.api+json'
                   })
        json_data = rv.get_json(force=True)
        assert len(json_data['data']) == topics_generated % 20
