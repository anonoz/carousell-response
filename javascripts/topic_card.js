import React from 'react';

export class TopicCard extends React.Component {
  constructor(props) {
    super(props);

    this.handleUpvoteClick = this.handleUpvoteClick.bind(this);
    this.handleDownvoteClick = this.handleDownvoteClick.bind(this);
  }

  handleUpvoteClick() {
    fetch(`/topics/${this.props.id}/upvote`, {
      method: 'post',
      headers: new Headers({
        'Content-Type': 'application/vnd.api+json',
        'Accept': 'application/vnd.api+json'
      })
    }).then(response => {
      response.json().then(response_data => {
        this.props.onUpvoteDownvote(response_data.data);
      })
    })
  }

  handleDownvoteClick() {
    fetch(`/topics/${this.props.id}/downvote`, {
      method: 'post',
      headers: new Headers({
        'Accept': 'application/vnd.api+json'
      })
    }).then(response => {
      response.json().then(response_data => {
        this.props.onUpvoteDownvote(response_data.data);
      })
    })
  }

  render() {
    return (
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="card-body">
              <div className="row">
                <div className="col-3">
                  <div onClick={this.handleUpvoteClick}
                    className="btn btn-primary">Upvote</div>
                  <span>{ this.props.topic.karma }</span>
                  <div onClick={this.handleDownvoteClick}
                    className="btn btn-secondary">Downvote</div>    
                </div>
                <div className="col-9">
                  <p>{ this.props.topic.content }</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
