import React from 'react';

export default class TopicForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      content: ''
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    fetch('/topics/', {
      method: 'post',
      headers: new Headers({
        'Content-Type': 'application/vnd.api+json',
        'Accept': 'application/vnd.api+json'
      }),
      body: JSON.stringify({
        data: {
          type: 'topics',
          attributes: {
            content: this.state.content
          }
        },
      })
    }).then(response => {
      response.json().then(response_data => {
        this.props.onTopicSubmit(response_data.data);
      })

      this.setState({
        content: ''
      })
    })
  }

  render() {
    return (
      <div className="card">
        <div className="card-body">
          <form onSubmit={this.handleSubmit} className="form-group" >
            <textarea onChange={this.handleInputChange} 
              name="content"
              maxLength="255"
              className="form-control" />
            <button className="btn btn-success">
              Save
            </button>
          </form>

        </div>
      </div>
    );
  }
}
