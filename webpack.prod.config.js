const path = require('path');

module.exports = {
  mode: 'production',
  entry: './javascripts/index.js',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, 'static/dist'),
    filename: 'index.bundle.js'
  }
};
