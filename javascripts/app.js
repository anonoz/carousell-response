import _ from 'lodash';
import React from 'react';
import {TopicCard} from './topic_card.js';
import TopicForm from './topic_form';

export class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      topics: []
    }

    this.handleTopicSubmit = this.handleTopicSubmit.bind(this)
    this.handleUpvoteDownvote = this.handleUpvoteDownvote.bind(this)
  }

  componentDidMount() {
    this.fetchTopics().then(response => {
      response.json().then(x => {
        console.log(x);
        this.setState({
          topics: x.data
        });
      });
    });
  }

  fetchTopics() {
    return fetch('/topics/', {
      headers: new Headers({
        'Accept': 'application/vnd.api+json'
      })
    });
  }

  handleTopicSubmit(newTopic) {
    this.setState({
      topics: this.state.topics.concat([newTopic])
    });
  }

  handleUpvoteDownvote(topic) {
    let topicIndex = _.findIndex(this.state.topics, {id: topic.id});
    let newTopics = this.state.topics.slice();
    newTopics.splice(topicIndex, 1, topic);
    this.setState({
      topics: newTopics
    });
  }

  render() {
    this.listTopics =  this.state.topics.map((topic) => 
      <TopicCard key={topic.id} id={topic.id}
        topic={topic.attributes}
        onUpvoteDownvote={this.handleUpvoteDownvote} />
    )

    return (
      <div>
        {this.listTopics}
        <hr />
        <TopicForm onTopicSubmit={this.handleTopicSubmit} />
      </div>
    ); 
  }
}
